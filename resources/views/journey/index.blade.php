@extends('layouts.app')
@section('title', 'Home')

@section('content')
    <div class="row h-100">
        <div class="col-md-12 text-center border rounded bg-dark p-3">
            <div class="row gy-3" id="journeys-list" style="overflow-y: scroll;">
                @foreach ($journeys as $journey)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header text-start">
                                <a class="btn btn-sm float-end btn-danger"
                                   href="{{url('journeys/'.$journey->id.'/delete')}}">
                                    <i class="fas fa-trash"></i>
                                </a>
                                <a class="btn btn-sm float-end btn-success mx-2"
                                   href="{{url('journeys/'.$journey->id.'/clone')}}">
                                    <i class="fas fa-clone text-white"></i>
                                </a>
                                <a class="btn btn-sm float-end btn-primary"
                                   href="{{url('journeys/'.$journey->id)}}">
                                    <i class="fas fa-pen-square"></i>
                                </a>
                                <h5>{{$journey->user->name}}</h5>
                            </div>
                            <div class="card-body">
                                <div style="overflow-y: scroll; height: 250px;">
                                    @foreach ($journey->steps()->where('step_type', 'location')->orderBy('step_order')->get() as $step)
                                        <div class="card">
                                            <div class="card-body">
                                                <h3>{{$step->name}}</h3>
                                            </div>
                                        </div>
                                        @if (!$loop->last)
                                            <div>
                                                <img src="{{asset('img/freccia_01.png')}}" alt="" width="60"
                                                     height="40">
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="card-footer text-muted">
                                {{date('d/m/Y H:i:s', strtotime($journey->updated_at))}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/search.js')}}"></script>
@endsection
