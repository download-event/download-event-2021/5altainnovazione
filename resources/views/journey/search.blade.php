@extends('layouts.app')
@section('title', 'Home')

@section('content')
    <div class="row h-100">
        <div class="col-lg-2 bg-dark">
            <div class="text-center">
                <input class="form-control me-2 my-2" type="search" placeholder="Italy,France,Germany"
                       aria-label="Search" id="jSearch" value="{{$q ?? ""}}">
                <div class="d-grid gap-2 mt-2">
                    <button class="btn btn-success" type="button" id="btnJSearch">Search</button>
                </div>

                <h2 class="text-light mt-4">Filters</h2>
                <div class="text-center border rounded p-3">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault"
                               id="flexRadioDefault1">
                        <label class="form-check-label text-light" for="flexRadioDefault1">
                            Popularity
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault"
                               id="flexRadioDefault2">
                        <label class="form-check-label text-light" for="flexRadioDefault2">
                            Date
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault"
                               id="flexRadioDefault2">
                        <label class="form-check-label text-light" for="flexRadioDefault2">
                            Rating
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-10 text-center border rounded bg-dark p-3">
            <div class="row gy-3" id="journeys-list" style="overflow-y: scroll;">
                @foreach ($journeys as $journey)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header text-start">
                                <a class="btn btn-sm float-end btn-primary"
                                   href="{{url('journeys/'.$journey->id.'/clone')}}">
                                    <i class="fas fa-folder-plus"></i>
                                </a>
                                <h5>{{$journey->user->name}}</h5>
                            </div>
                            <div class="card-body">
                                <div style="overflow-y: scroll; height: 250px;">
                                    @foreach ($journey->steps()->where('step_type', 'location')->orderBy('step_order')->get() as $step)
                                        <div class="card">
                                            <div class="card-body">
                                                <h3>{{$step->name}}</h3>
                                            </div>
                                        </div>
                                        @if (!$loop->last)
                                            <div>
                                                <img src="{{asset('img/freccia_01.png')}}" alt="" width="60"
                                                     height="40">
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="card-footer text-muted">
                                {{date('d/m/Y H:i:s', strtotime($journey->updated_at))}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/search.js')}}"></script>
@endsection
