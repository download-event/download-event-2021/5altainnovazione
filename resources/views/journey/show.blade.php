@extends('layouts.app')
@section('title', 'Home')

@section('content')
    <div class="row h-100">
        <div class="col-lg-8 h-100" id="map">
            <div class="leaflet-top leaflet-right">
                <button type="button" id="btnModalJ" class="btn d-none" data-bs-toggle="modal"
                        data-bs-target="#modalJourney" style="pointer-events: auto;">
                    <img src="{{asset('img/menu_01.png')}}" style="width: 50px; height: 50px; " alt="">
                </button>
            </div>
        </div>
        <div class="modal fade" id="modalJourney" tabindex="-1" aria-labelledby="modalJLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalJLabel">Journey settings</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox"
                                   id="jSwitchPublic" {{$journey->public ? "checked" : ""}}>
                            <label class="form-check-label" for="jSwitchPublic">Public</label>
                        </div>
                        <hr>
                        <h5>Friends (can edit):</h5>
                        <p>
                            @foreach($journey->friends as $friend)
                                {{$friend->name}} ({{$friend->email}}) <br>
                            @endforeach
                        </p>
                        <div class="form-group">
                            <label for="JFemail">Add new friend</label>
                            <input type="email" id="JFemail" class="form-control" placeholder="Friend's email">
                            <div class="d-grid gap-2 mt-2">
                                <button type="button" class="btn btn-success" id="btnJF">Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 text-center border border-success border-3 rounded" id="journeySteps"
             style="overflow-y: scroll;">
            <h1>Itinerary</h1>
            <form action="{{url('journeys/'.$journey->id)}}" method="post">
                @csrf
                @method('PUT')
                <div class="btn-group mb-2" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-info" disabled><i class="fas fa-cloud-download-alt"></i> Download
                    </button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save</button>
                </div>
                @foreach ($journey->steps as $step)
                    @if ($step->step_type == 'location')
                        <div class="card">
                            <div class="card-body">
                                <div class="float-end">
                                    <a href="{{url('journeys/'.$journey->id.'/remove-step?id='.$step->id)}}"
                                       type="button"
                                       class="btn-close" aria-label="Close"></a>
                                </div>
                                <h2>{{$step->name}}</h2>
                                <textarea class="form-control"
                                          name="step_{{$step->id}}">{{$step->note->value}}</textarea>
                            </div>
                        </div>
                    @elseif ($step->step_type == 'stretch')
                        <div class="row">
                            <div class="col-2">
                                <img src="{{asset('img/freccia-blu.png')}}" alt="" width="60" height="120">
                            </div>
                            <div class="col-10">
                                <div class="card ">
                                    <div class="card-body">
                                        <div class="float-end">
                                            <a href="{{url('journeys/'.$journey->id.'/remove-step?id='.$step->id)}}"
                                               type="button"
                                               class="btn-close" aria-label="Close"></a>
                                        </div>
                                        <h2><i>Movement:</i> {{$step->name}}</h2>
                                        <textarea class="form-control"
                                                  name="step_{{$step->id}}">{{$step->note->value}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/journeyEditor.js')}}"></script>
@endsection
