@extends("layouts.app")

@section('content')
    <div class="col-md-8 mx-auto">
        <div class="m-auto w-4/5 py-24">
            <div class="text-center">
                <h1 class="text-5xl text-uppercase text-bold">{{config("app.name")}}</h1>
            </div>
        </div>
        <div class="w-5/6 py-10 text-danger" style="font-size: large;">
            You must be logged in to proceed.
        </div>
    </div>
@endsection
