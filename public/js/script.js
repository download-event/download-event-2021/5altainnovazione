function fixHeight() {
    let navbar = document.getElementsByClassName('navbar')[0];
    let footer = document.getElementsByTagName('footer')[0];
    let remaining = window.innerHeight - navbar.offsetHeight - footer.offsetHeight;
    document.getElementById('appContainer').style.height = remaining + 'px';
    if (document.getElementById('journeySteps'))
        document.getElementById('journeySteps').style.height = remaining + 'px';
    if (document.getElementById('journeys-list'))
        document.getElementById('journeys-list').style.height = remaining + 'px';
    if (document.getElementById('btnModalJ') && document.getElementById('btnModalJ').classList.contains('d-none'))
        document.getElementById('btnModalJ').classList.remove('d-none');
}

fixHeight();

window.onresize = fixHeight;
