const journeyId = window.location.toString().split('/')[4].replace('?openmodal=1', '');

window.onload = () => {
    if (window.location.toString().includes('openmodal=1')) {
        jQuery('#modalJourney').modal('show');
    }
}

let map = L.map('map').setView([42.0365314, 11.9044681], 4);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    minZoom: 1,
    //maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1,
    noWrap: true,
}).addTo(map);

map.setMaxBounds([[-90, -180], [90, 180]])
let geojson;

let selectedCountry;

function style(feature) {
    return {
        weight: 2,
        opacity: 0.1,
        color: 'black',
        fillOpacity: 0.0,
    };
}

function highlightFeature(e) {
    let layer = e.target;

    layer.setStyle({
        weight: 7,
        color: '#2378d9',
        fillOpacity: 0.5
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

function resetHighlight(e) {
    geojson.resetStyle(e.target);
}

function OnMapCLick(e) {
    map.fitBounds(e.target.getBounds());
    selectedCountry = e.target.feature.properties.name;
    window.location = window.location.toString().replace('?openmodal=1', '') + '/add-step?name=' + selectedCountry;
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: OnMapCLick
    });
}

geojson = L.geoJson(statesData, {
    onEachFeature: onEachFeature,
    style: style,
}).addTo(map);

function OnSaveCLick() {

}

document.getElementById('jSwitchPublic').addEventListener('click', () => {
    jQuery.post('/api/journeys/' + journeyId, {
        _token: jQuery('meta[name="csrf-token"]').attr('content'),
        public: document.getElementById('jSwitchPublic').checked
    });
});

document.getElementById('btnJF').addEventListener('click', () => {
    jQuery.post('/api/journeys/' + journeyId + '/add-friend', {
        _token: jQuery('meta[name="csrf-token"]').attr('content'),
        newFriendEmail: document.getElementById('JFemail').value
    }).done(function (data) {
        if (!data)
            document.getElementById('JFemail').classList.add('is-invalid');
        else if (window.location.toString().includes("?openmodal=1"))
            location.reload();
        else
            window.location = window.location.href + "?openmodal=1";
    });
});

document.getElementById('JFemail').addEventListener('change', () => {
    if (document.getElementById('JFemail').classList.contains('is-invalid'))
        document.getElementById('JFemail').classList.remove('is-invalid');
});
document.getElementById('JFemail').addEventListener('click', () => {
    if (document.getElementById('JFemail').classList.contains('is-invalid'))
        document.getElementById('JFemail').classList.remove('is-invalid');
});
