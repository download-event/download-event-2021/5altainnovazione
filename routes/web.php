<?php

use App\Http\Controllers\JourneyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get("/", 'App\Http\Controllers\HomeController@index');
    Route::resource('journeys', JourneyController::class)->only(['index', 'create', 'show', 'update']);
    Route::get('my-journeys', 'App\Http\Controllers\JourneyController@myJourneys');
    Route::get('search', 'App\Http\Controllers\JourneyController@search');
    Route::get('journeys/{journey}/add-step', 'App\Http\Controllers\JourneyController@addStep');
    Route::get('journeys/{journey}/clone', 'App\Http\Controllers\JourneyController@clone');
    Route::get('journeys/{journey}/remove-step', 'App\Http\Controllers\JourneyController@removeStep');
    Route::get('journeys/{journey}/delete', 'App\Http\Controllers\JourneyController@destroy');
});

Auth::routes([
    'register' => true, // Registration Routes...
    'reset' => true, // Password Reset Routes...
    'verify' => true, // Email Verification Routes...
]);
