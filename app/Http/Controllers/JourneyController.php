<?php

namespace App\Http\Controllers;

use App\Models\Journey;
use App\Models\Note;
use App\Models\Step;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Str;

class JourneyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Redirector|RedirectResponse
     */
    public function index(): Redirector|RedirectResponse|Application
    {
        return redirect('my-journeys');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function create(Request $request): Redirector|RedirectResponse|Application
    {
        $journey = Journey::create([
            'id' => Str::uuid(),
            'user_id' => $request->user()->id
        ]);
        return redirect('journeys/' . $journey->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Journey $journey
     * @return Application|Factory|View
     */
    public function show(Journey $journey): View|Factory|Application
    {
        return view('journey.show', compact('journey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Journey $journey
     * @return \Illuminate\Http\Response
     */
    public function edit(Journey $journey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Journey $journey
     * @return Application|Redirector|RedirectResponse
     */
    public function update(Request $request, Journey $journey): Redirector|RedirectResponse|Application
    {
        $inputs = $request->all();
        foreach ($inputs as $key => $value) {
            if (stripos($key, 'step_') !== 0)
                continue;
            if (empty(trim($value)))
                continue;
            $stepId = str_replace('step_', '', $key);
            $step = Step::find($stepId);
            if ($step->note()->count() == 0)
                $step->note->create(['id' => Str::uuid(), 'step_id' => $stepId, 'user_id' => $request->user()->id, 'value' => $value]);
            else
                $step->note->update(['value' => $value]);
            $step->save();
        }
        return redirect('journeys/' . $journey->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Journey $journey
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy(Journey $journey): Redirector|RedirectResponse|Application
    {
        $journey->delete();
        return redirect('my-journeys');
    }

    public function addStep(Request $request, Journey $journey): Redirector|Application|RedirectResponse
    {
        if (is_null($request->input('name')))
            return redirect('journeys/' . $journey->id);

        if ($journey->steps()->count() > 0)
            $journey->steps()->create(['id' => Str::uuid(), 'step_order' => $journey->steps()->count() + 1, 'name' => '', 'step_type' => 'stretch']);
        $journey->steps()->create(['id' => Str::uuid(), 'name' => $request->input('name'), 'step_order' => $journey->steps()->count() + 1]);
        return redirect('journeys/' . $journey->id);
    }

    public function removeStep(Request $request, Journey $journey): Redirector|Application|RedirectResponse
    {
        if (is_null($request->input('id')))
            return redirect('journeys/' . $journey->id);

        $journey->steps()->findOrFail($request->input('id'))->delete();
        return redirect('journeys/' . $journey->id);
    }

    public function apiEdit(Request $request, Journey $journey): Journey
    {
        if (!is_null($request->input('public'))) {
            $journey->public = $request->input('public') === "true" ? 1 : 0;
            $journey->save();
        }
        return $journey;
    }


    public function addFriend(Request $request, Journey $journey): ?User
    {
        if (is_null($request->input('newFriendEmail')))
            return null;
        $friend = User::where('email', $request->input('newFriendEmail'));
        if ($friend->exists() == false)
            return null;
        $friend = $friend->first();
        if ($friend->id == $journey->user->id)
            return null;
        if ($journey->friends()->find($friend->id))
            return null;
        $journey->friends()->attach($friend->id);
        return $friend;
    }

    private function searchByQuery(?string $q): \Illuminate\Support\Collection
    {
        if (empty($q))
            return Journey::where('public', 1)->has('steps')->get();
        else {
            $result = collect();
            $steps = explode(',', $q);
            $steps = array_map('trim', $steps);
            $allJourneys = Journey::where('public', 1)->get();
            foreach ($allJourneys as $journey) {
                if ($journey->steps()->count() == 0)
                    continue;
                $stepsArray = $journey->steps()->pluck('name')->toArray();
                $stepsArray = array_map('strtolower', $stepsArray);
                foreach ($steps as $step)
                    if (in_array(strtolower($step), $stepsArray) == false)
                        continue 2;
                $result->add($journey);
            }
            return $result;
        }
    }

    public function search(Request $request): Factory|View|Application
    {
        $q = $request->input('q');
        $journeys = $this->searchByQuery($q);
        return view('journey.search', compact('journeys', 'q'));
    }

    public function clone(Request $request, Journey $journey): Redirector|Application|RedirectResponse
    {
        $newJourney = Journey::create(['id' => Str::uuid(), 'user_id' => $request->user()->id]);
        foreach ($journey->steps as $step) {
            $newStep = $newJourney->steps()->create([
                'id' => Str::uuid(),
                'name' => $step->name,
                'journey_id' => $newJourney->id,
                'step_type' => $step->step_type,
                'step_order' => $step->step_order
            ]);
            if (!empty($step->note->id))
                Note::create([
                    'id' => Str::uuid(),
                    'user_id' => $request->user()->id,
                    'value' => $step->note->value,
                    'step_id' => $newStep->id
                ]);
        }
        return redirect('journeys/' . $newJourney->id);
    }

    function myJourneys(Request $request): Factory|View|Application
    {
        $journeys = $request->user()->journeys()->has('steps')->get();
        $friendsJourneys = $request->user()->friendsJourneys();
        $journeys = $journeys->merge($friendsJourneys);
        return view('journey.index', compact('journeys'));
    }
}
