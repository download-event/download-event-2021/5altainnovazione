<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Journey
 *
 * @property string $id
 * @property int $user_id
 * @property int $public
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Journey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Journey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Journey query()
 * @method static \Illuminate\Database\Eloquent\Builder|Journey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journey wherePublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journey whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journey whereUserId($value)
 * @mixin \Eloquent
 */
class Journey extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = ['id', 'user_id', 'public'];

    public function notes(): BelongsToMany
    {
        return $this->belongsToMany(Note::class)->withPivot('user_id');
    }

    public function steps(): HasMany
    {
        return $this->hasMany(Step::class)->orderBy('step_order');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function friends(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
