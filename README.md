# 5AltaInnovazione - https://quovado.oversamu.cf

## Il progetto
[QuoVado](https://quovado.oversamu.cf) è un sito web realizzato da 5AltaInnovazione con l'intento di semplificare l'organizzazione di un viaggio, soprattutto in questo periodo. É dotato di una mappa interattiva, implementata con le API Leaflet e GeoJSON, le quali permettono di scegliere ogni tappa del proprio viaggio con un solo click. Dopo aver cliccato, sul pannello laterale apparirà una card con il nome del paese cliccato e, nel caso non fosse il primo, anche una card sullo spostamento. Ognuna di esse è dotata di una textarea ridimensionabile nella quale poter scrivere delle note di qualsiasi tipo.
Una volta che si ha finito, si può comodamente salvare l'itinerario sul server.

Nella parte in alto a destra della mappa, si può trovare un pulsante che mostra due opzioni:
Nel caso il viaggio sia organizzato da più persone, esse possono essere invitate a collaborare allo sviluppo dell'itinerario tramite mail oppure, se si vuole rendere il proprio itinerario pubblico, lo si può addirittura fare tramite un comodo toggle switch.

Se si decide di farlo, esso apparirà nella pagina "Search" del sito web, accessibile dalla navbar. In questa pagina è possibile trovare tutti gli itinerari pubblici condivisi dagli altri utenti ed è possibile importarli nel proprio profilo e modificarli.
Si possono anche filtrare per paese, per una ricerca più semplice e comoda in base alle proprie esigenze.

Tutti i propri viaggi potranno essere trovati nella pagina "My Journeys", dove è possibile eliminarli, duplicarli o modificarli.

Per utilizzare QuoVado bisogna eseguire il login. Come di consueto, sul form si trova una checkbox "Remember me" e anche un collegamento per resettare la password nel caso essa venga dimenticata.
Se non si è loggati e si accede al sito, si verrà accolti da un form per loggarsi oppure, se non si possiede un account, si può andare alla pagina di registrazione tramite l'apposito collegamento in alto a destra.

## Come provare il software
È possibile utilizzare l'applicazione da noi realizzata tramite il sito web https://quovado.oversamu.cf/. 
Ci si può registrare liberamente, altrimenti utilizzare le credenziali `test@example.com/test`. 

In alternativa si può scaricare la repository, eseguire i comandi `composer install`, `npm install`, copiare il file `.env.example` in `.env`, eseguire il comando `php artisan key:generate`, inserire le informazioni riguardanti il proprio database MySQL dove verranno memorizzati i dati ed avviare il proprio webserver (noi utilizziamo Apache) inserendo come root_directory la cartella `public`. 

## Le tecnologie utilizzate
- PHP
- Framework Laravel (open-source)
- MySQL
- Bootstrap
- Javascript
- CSS

## Autori

- Giorgio Cucco
- Alessandro Gioia
- Mattia Marra
- Alberto Ravasio
- Samuele Riva
